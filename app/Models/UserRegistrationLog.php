<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRegistrationLog extends Model
{
    use HasFactory;
    protected $table = 'user_reg_log';

    protected $fillable = [
        'user_id',
        'user_log',
    ];
}
